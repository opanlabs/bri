$( document ).ready(function() {

	$('a.sub[href^="#"]').on('click', function(e) {
        e.preventDefault();

        var target = this.hash,
                $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function() {
            window.location.hash = target;
        });
    });
    

	$(window).scroll(function(){
	    if ($(window).scrollTop() >= 100) {
	        $('#top').addClass('fixed-header');
	    }
	    else {
	        $('#top').removeClass('fixed-header');
	    }
	});


	//slider home
	$('.single-item').slick({
	  infinite: true,
	  dots: true,	  
	  autoplay: true,
  	  autoplaySpeed: 2000,
      arrows: false
	});

	var accordionsMenu = $('.cd-accordion-menu');

	if( accordionsMenu.length > 0 ) {
		
		accordionsMenu.each(function(){
			var accordion = $(this);
			//detect change in the input[type="checkbox"] value
			accordion.on('change', 'input[type="radio"]', function(){
				var checkbox = $(this);
				console.log(checkbox.prop('checked'));
				console.log(checkbox.parent().next());
				( checkbox.prop('checked') ) ? checkbox.parent().next().slideDown('slow') : checkbox.parent().next().slideUp('slow');
			});
		});
	}

	$('.slider-riwayat').slick({
	  infinite: true,
	  dots: false,	  
      prevArrow: '<div class="prev"><img src="images/arrow-kiri.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan.png" /></div>'
	});

	$('.komite-slider').slick({
	  infinite: true,
	  dots: false,	  
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});

	$('.whistle-slider').slick({
	  infinite: true,
	  dots: false,	  
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});

	$('.fraud-slider').slick({
	  infinite: true,
	  dots: false,	  
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});

	$('.x-slide').slick({
	  infinite: true,
	  dots: false,	  
	  slidesToShow: 5,
	  slidesToScroll: 1,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>',
      responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.cerita-slider').slick({
	  infinite: false,
	  dots: true,	  
	  slidesToShow: 4,
	  slidesToScroll: 4,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>',
      responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.file-slider').slick({
	  infinite: true,
	  dots: false,	  
	  slidesToShow: 5,
	  slidesToScroll: 1,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>',
      responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.file-slider2').slick({
	  infinite: true,
	  dots: false,	  
	  slidesToShow: 9,
	  slidesToScroll: 1,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>',
      responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('#collapseOne').on('show.bs.collapse', function(){
	    $('.x-slide').slick('refresh');
	});
	$('#collapseTwo').on('show.bs.collapse', function(){
	    $('.x-slide').slick('refresh');
	});

	$('.management-slider').slick({
	  infinite: true,
	  dots: false,	  
	  adaptiveHeight: true,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});

	$('.berkarir-slider').slick({
	  infinite: true,
	  dots: false,	  
	  adaptiveHeight: true,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan.png" /></div>'
	});

	$('.traffic-slider').slick({
	  infinite: true,
	  dots: false,	  
	  adaptiveHeight: true,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});

	$('.nilai-slider').slick({
	  infinite: true,
	  dots: false,	  
	  centerMode: true,
      centerPadding: '40px',
  	  slidesToShow: 1,
  	  adaptiveHeight: true,
      prevArrow: '<div class="prev"><img src="images/arrow-kiri-2.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan-2.png" /></div>'
	});
	
	$('.nav-tabs a').click(function(){
        $(this).tab('show');
    });

    $('.award-slider ').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  dots: false,	  
      prevArrow: '<div class="prev"><img src="images/arrow-kiri.png" /></div>',
      nextArrow: '<div class="next"><img src="images/arrow-kanan.png" /></div>',
      responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.check').click(function(){
        $('.check').removeClass('active');
        $(this).addClass('active');
    });
	
	function activaTab(tab){
	    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
	    $('.x-slide').slick('refresh');
	};

	activaTab('layanan-nasabah');

});


function openNav() {
    document.getElementById('mySidenav').style.width = '70%';
}

function closeNav() {
    document.getElementById('mySidenav').style.width = '0';
}